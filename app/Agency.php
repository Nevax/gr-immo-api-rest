<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'agencies';

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function owners()
    {
        return $this->hasMany('App\User', 'owner_agency_id');
    }

    public function agents()
    {
        return $this->hasMany('App\User', 'agent_agency_id');
    }

    public function buyer()
    {
        return $this->hasMany('App\User', 'buyer_agency_id');
    }
}
