<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'properties';

    // ---------- Define relationships ----------- //
    public function rooms() {
        return $this->hasMany('App\Room');
    }

    public function type()
    {
        return $this->belongsTo('App\PropertyType','property_type_id');
    }

    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

    public function agency(){
        return $this->belongsTo('App\Agency');
    }

    public function owner(){
        return $this->belongsTo('App\User','owner_id');
    }

    public function agent(){
        return $this->belongsTo('App\User','agent_id');
    }

    public function buyer(){
        return $this->belongsTo('App\User','buyer_id');
    }
}
