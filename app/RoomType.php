<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    // Room <-> RoomType
    public function rooms()
    {
        return $this->hasMany('App\RoomType');
    }
}
