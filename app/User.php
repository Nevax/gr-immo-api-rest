<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Agency <-> User
    public function ownerAgency(){
        return $this->belongsTo('App\Agency', 'owner_agency_id');
    }

    public function buyerAgency(){
        return $this->belongsTo('App\Agency', 'buyer_agency_id');
    }

    public function agentAgency(){
        return $this->belongsTo('App\Agency', 'agent_agency_id');
    }

    // UserResource <-> UserResource
    public function chief(){
        return $this->hasMany('App\User', 'agent_chief_id');
    }

    public function directed(){
        return $this->belongsTo('App\User', 'agent_chief_id');
    }

    //
    public function propertiesAsOwner()
    {
        return $this->hasMany('App\Property', 'owner_id');
    }

}
