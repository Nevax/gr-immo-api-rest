<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'appointments';

    // ---------- Define relationships ----------- //

    public function property()
    {
        return $this->belongsTo('App\Property');
    }
}
