<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'property_types';

    // ---------- Define relationships ----------- //
    // Property <-> PropertyType 
    public function properties()
    {
        return $this->hasMany('App\Property');
    }

}
