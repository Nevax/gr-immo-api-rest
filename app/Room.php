<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array();

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'rooms';

    // ---------- Define relationships ----------- //

    // Property <-> Room
    public function property()
    {
        return $this->belongsTo('App\Property', 'property_id');
    }

    // Photos <-> Room
    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    // RoomType <-> Room
    public function type()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }
}
