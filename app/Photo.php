<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class photo extends Model
{

    /**
     * Link this model to our database model
     *
     * @var string
     */
    protected $table = 'photos';

    // ---------- Define relationships ----------- //

    // Photo <-> Room
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id');
    }
}
