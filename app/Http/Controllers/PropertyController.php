<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

use App\Http\Resources\PropertyResource;
use App\Http\Resources\PropertiesResource;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = Property::with(['type', 'rooms', 'photos']);
        return (new PropertiesResource($properties))->paginate();
    }

    public function show(Property $property)
    {
        //return $property;
        return new PropertyResource($property);
    }

    public function store(Request $request)
    {
        $property = Property::create($request->all());

        return response()->json($property, 201);
    }

    public function update(Request $request, Property $property)
    {
        $property->update($request->all());

        return response()->json($property, 200);
    }

    public function delete(Property $property)
    {
        $property->delete();

        return response()->json(null, 204);
    }
}
