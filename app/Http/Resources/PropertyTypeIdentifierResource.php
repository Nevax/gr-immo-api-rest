<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PropertyTypeIdentifierResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'property_type',
            'id'         => (string)$this->id,
//            'links' => [
//                'self' => route('type.show', ['type' => $this->id]),
//            ]
        ];
    }
}
