<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RoomsIdentifierResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'room',
            'id' => (string)$this->id,
            'attributes' => [
                'description' => $this->description,
                'size' => $this->size,
            ],
            'relationships' => new RoomsRelationshipResource($this),
        ];
    }
}
