<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomPhotosRelationshipResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $room = $this->additional['room'];
        return [
            'data'  => PhotoIdentifierResource::collection($this->collection),
            'links' => [
//                'self'    => route('room.relationships.photos', ['room' => $room->id]),
//                'related' => route('room.photos', ['room' => $room->id]),
            ],
        ];
    }
}
