<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'type' => 'users',
            'id' => (string)$this->id,
            'attributes' => [
                'username' => $this->username,
                'email' => $this->email,
                'last_name' => $this->last_name,
                'first_name' => $this->first_name,
                'phone' => $this->phone,
                'address' => $this->address,
                'zip_code' => $this->zip_code,
                'city' => $this->city,
            ],
            'relationships' => null,
            'links' => [
                'self' => route('user.show', ['user' => $this->id]),
            ]
            // 'profile' => url('/users/' . $this->id . '/'),

        ];
    }

    public function with($request)
    {
        return [
            'version' => '1.0.0',
            'attribution' => url('/terms-of-service'),
            'valid_as_of' => date('D;d M Y H:i:s'),
        ];
    }
}
