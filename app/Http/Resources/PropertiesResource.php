<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertiesResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PropertyResource::collection($this->collection),
        ];
    }

    public function with($request)
    {
//        return [
//            'version' => '1.0.0',
//            'attribution' => url('/terms-of-service'),
//            'valid_as_of' => date('D;d M Y H:i:s'),
//        ];
        return [
            'links'    => [
                'self' => route('property.index'),
            ],
        ];
    }
}
