<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PropertyRelationshipResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => [
                'links' => [
                    'self' => null, # route('property.relationships.type', ['property' => $this->id]),
                    'related' => null, # route('property.type', ['property' => $this->id]),
                ],
                'data' => new PropertyTypeIdentifierResource($this->type),
            ],
            'rooms' => (new PropertyRoomsRelationshipResource($this->rooms))->additional(['property' => $this]),
            'owner' => new UserIdentifierResource($this->owner),

            'interested' => null,
        ];
    }

    public function with($request)
    {
        return [
            'links' => [
                'self' => route('property.index'),
            ],
        ];
    }
}
