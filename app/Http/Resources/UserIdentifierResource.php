<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserIdentifierResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'       => 'user',
            'id'         => (string)$this->id,
            'links' => [
                'self' => route('user.show', ['user' => $this->id]),
            ]
        ];
    }
}
