<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RoomsRelationshipResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => [
                'links' => [
                    'self' => null, # route('room.relationships.type', ['room' => $this->id]),
                    'related' => null, # route('room.type', ['room' => $this->id]),
                ],
                'data' => new RoomRoomTypeIdentifierResource($this->type),
            ],
            //dd($this->rooms),
            //'rooms' => (new PropertyRoomsRelationshipResource($this->rooms))->additional(['property' => $this]),
            //dd($this->photos),
            'photos' => (new RoomPhotosRelationshipResource($this->photos))->additional(['room' => $this]),

        ];
    }

    public function with($request)
    {
        return [
            'links' => [
                //'self' => route('room.index'),
            ],
        ];
    }
}
