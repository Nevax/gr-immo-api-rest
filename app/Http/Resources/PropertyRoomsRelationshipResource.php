<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyRoomsRelationshipResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $property = $this->additional['property'];
        return [
            'data'  => RoomsIdentifierResource::collection($this->collection),
            'links' => [
//                'self'    => route('property.relationships.rooms', ['property' => $property->id]),
//                'related' => route('property.rooms', ['property' => $property->id]),
            ],
        ];
    }
}
