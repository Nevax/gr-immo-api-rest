<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RoomTypeIdentifierResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'room_type',
            'id' => (string)$this->id,
            'name' => $this->name,
//            'links' => [
//                'self' => route('type.show', ['type' => $this->id]),
//            ]
        ];
    }
}
