<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PropertyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'type' => 'property',
            'id' => (string)$this->id,
            'attributes' => [
                'name' => $this->name,
                'description' => $this->description,
                'size' => $this->size,
                'isSold' => $this->isSold,
                'price' => $this->price,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
            ],
            'relationships' => new PropertyRelationshipResource($this),
            'links' => [
                'self' => route('property.show', ['property' => $this->id]),
            ]
            // 'profile' => url('/users/' . $this->id . '/'),

        ];
    }


//    public function with($request)
//    {
//        $rooms = $this->collection->flatMap(
//            function ($property) {
//                return $property->rooms;
//            }
//        );
//        $types  = $this->collection->map(
//            function ($property) {
//                return $property->type;
//            }
//        );
//        $included = $types->merge($rooms)->unique();
//
//        return [
//            'links'    => [
//                'self' => null, #route('type.index'),
//            ],
//            'included' => $this->withIncluded($included),
//        ];
//    }

//    private function withIncluded(Collection $included)
//    {
//        return $included->map(
//            function ($include) {
//                if ($include instanceof People) {
//                    return new PeopleResource($include);
//                }
//                if ($include instanceof Comment) {
//                    return new CommentResource($include);
//                }
//            }
//        );
//    }
}
