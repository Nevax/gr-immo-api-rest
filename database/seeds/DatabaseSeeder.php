<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AgencyTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PaymentModesTableSeeder::class);
        $this->call(PropertyTypesTableSeeder::class);
        $this->call(PropertyTableSeeder::class);
        $this->call(SaleRecordsSeeder::class);
        $this->call(InvoicesTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(RoomTypesTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(PhotosTableSeeder::class);
        $this->call(AppointmentsTableSeeder::class);
    }
}
