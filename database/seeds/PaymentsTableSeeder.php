<?php

use Illuminate\Database\Seeder;

use App\Payment;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // And now, let's create a few properties in our database:
        for ($i = 1; $i <= 30; $i++) {
            Payment::create([
                'amount' => $faker->numberBetween(50, 500),
                'date' => $faker->date(),
                'invoices_id' => $faker->numberBetween(1,25),
                'payment_modes_id' => $faker->numberBetween(1, 10),
            ]);
        }
    }
}
