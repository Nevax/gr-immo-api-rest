<?php

use Illuminate\Database\Seeder;

use App\Invoice;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // And now, let's create a few properties in our database:
        for ($i = 1; $i <= 25; $i++) {
            Invoice::create([
                'date' => $faker->date(),
                'price' => $faker->numberBetween(500, 2000000),
                'delay' => $faker->numberBetween(1, 30),
                'sale_records_id' => $faker->numberBetween(1, 20),
            ]);
        }
    }
}
