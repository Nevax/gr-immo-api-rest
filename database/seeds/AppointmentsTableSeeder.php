<?php

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Create 10 appointments
        for ($i = 1; $i <= 10; $i++) {

            // Generate a start date for your appointment
            $starts_at = Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '+2 days', $endDate = '+1 week')->getTimeStamp());
            // Define the end by adding between 1 and 8 hours from the start
            $ends_at = Carbon::createFromFormat('Y-m-d H:i:s', $starts_at)->addHours($faker->numberBetween(1, 8));

            Appointment::create([
                'begin' => $starts_at,
                'end' => $ends_at,
                'validation' => $faker->boolean,
                'sale_record_id' => $faker->numberBetween(1,10),
            ]);
        }
    }
}

