<?php

use Illuminate\Database\Seeder;
use App\Agency;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=1;$i<=10;$i++){
            Agency::create([
                'name'=> $faker->company,
                'street'=> $faker->streetName,
                'zip_code'=> $faker->postcode,
                'city'=> $faker->city,
                'phone_number'=> $faker->phoneNumber,
            ]);
        }
    }
}
