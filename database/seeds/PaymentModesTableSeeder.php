<?php

use Illuminate\Database\Seeder;

use App\PaymentMode;

class PaymentModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($j = 1; $j <= 10; $j++) {
            PaymentMode::create([
                'mode' => $faker->creditCardType,
            ]);
        }
    }
}
