<?php

use App\RoomType;
use Illuminate\Database\Seeder;

class RoomTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Let's create 5 property types.
        for ($i = 1; $i <= 5; $i++) {
            RoomType::create([
                'name' => $faker->word,
                'description' => $faker->sentence,
            ]);
        }
    }
}
