<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        // User::truncate();

        $faker = \Faker\Factory::create();


        // Let's make sure everyone has the same password and
        // let's hash it before the loop, or else our seeder
        // will be too slow.
        $password = Hash::make('secret');

        User::create([
            'username' => 'administrator',
            'email' => 'admin@test.com',
            'password' => $password,
            'last_name' => $faker->lastName,
            'first_name' => $faker->firstName,
            'phone' => $faker->phoneNumber,
            'address' => $faker->address,
            'zip_code' => $faker->countryCode,
            'city' => $faker-> city,
        ]);

        // AGENTS
        // chef des agences

        for($i=1;$i<11;$i++) {
            User::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => $password,
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'zip_code' => $faker->countryCode,
                'city' => $faker->city,
                'is_owner' => 'false',
                'is_buyer' => 'false',
                'is_agent' => 'true',
                'agent_agency_id' => $i,
            ]);
        };
        for($i=2;$i<12;$i++){
        User::create([
            'username' => $faker->userName,
            'email' => $faker->email,
            'password' => $password,
            'last_name' => $faker->lastName,
            'first_name' => $faker->firstName,
            'phone' => $faker->phoneNumber,
            'address' => $faker->address,
            'zip_code' => $faker->countryCode,
            'city' => $faker-> city,
            'is_owner' => 'false',
            'is_buyer' => 'false',
            'is_agent' => 'true',
            'agent_agency_id' => '1',
            'agent_chief_id' => $i,
        ]);
        }
        for($i=0;$i<10; $i++){
            User::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => $password,
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'zip_code' => $faker->countryCode,
                'city' => $faker-> city,
                'is_owner' => 'false',
                'is_buyer' => 'false',
                'is_agent' => 'true',
                'agent_agency_id' => $faker->numberBetween(1,10),
                'agent_chief_id' => $faker->numberBetween(2,11),
            ]);
        }

        // PROPRIETAIRES
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => $password,
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'zip_code' => $faker->countryCode,
                'city' => $faker-> city,
                'is_owner' => 'true',
                'owner_agency_id' => $faker->numberBetween(1,10),
                'is_buyer' => 'false',
                'is_agent' => 'false',
            ]);
        }

        // ACHETEURS
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => $password,
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'zip_code' => $faker->countryCode,
                'city' => $faker-> city,
                'is_owner' => 'false',
                'is_buyer' => 'true',
                'buyer_agency_id' => $faker->numberBetween(1,10),
                'is_agent' => 'false',
            ]);
        }
    }
}
