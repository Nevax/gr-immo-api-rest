<?php

use App\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Let's create 5 property types.
        for ($i = 1; $i <= 5; $i++) {
            PropertyType::create([
                'name' => $faker->word,
                'description' => $faker->sentence,
            ]);
        }

    }
}
