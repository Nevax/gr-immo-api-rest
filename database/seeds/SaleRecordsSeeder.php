<?php

use Illuminate\Database\Seeder;
use App\SaleRecord;

class SaleRecordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            SaleRecord::create([
                'id' => $i,
                'type' => $faker->numberBetween(1,4),
                'status' => $faker->numberBetween(1,3),
                'sale_date' => $faker->date(),
                'agent_id' => $faker->numberBetween(12,31),
                'buyer_id' => $faker->numberBetween(42,51),
                'owner_id' => $faker->numberBetween(32,41),
                'property_id' => $faker->numberBetween(1,10),
            ]);
        }
    }
}
