<?php

use Illuminate\Database\Seeder;
use App\Photo;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 90; $i++) {
            for ($j = 1; $j <= 10; $j++) {
                Photo::create([
                    'description' => $faker->sentence,
                    'file_name' => $faker->word . '.jpg',
                    'room_id' => $i,
                ]);
            }
        }
    }
}
