<?php

use Illuminate\Database\Seeder;

use App\Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        // Let's create 10 rooms for each properties.
        for ($i = 1; $i <= 10; $i++) { // Property id
            for ($y = 1; $y < 10; $y++) { // Rooms
                Room::create([
                    'property_id' => $i,
                    'room_type_id' => $faker->numberBetween(1, 5),
                    'description' => $faker->paragraph,
                    'size' => $faker->numberBetween(5, 100),
                ]);
            }
        }
    }
}
