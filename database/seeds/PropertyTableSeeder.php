<?php

use App\Property;
use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // And now, let's create a few properties in our database:
        for ($i = 1; $i <= 10; $i++) {
            Property::create([
                'name' => $faker->word,
                'description' => $faker->paragraph,
                'size' => $faker->numberBetween(5,300),
                'isSold' => $faker->boolean,
                'latitude' => $faker->randomFloat(6, -10, 10),
                'longitude' => $faker->randomFloat(6, -10, 10),
                'price' => $faker->numberBetween(1000,2000000),
                'property_type_id' => $faker->numberBetween(1,5),
                'agency_id' => $faker->numberBetween(1,10),
                'owner_id' => $faker->numberBetween(32,41),
            ]);
        }
    }
}