<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomRoomPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_room_pivot', function (Blueprint $table) {
            $table->integer('room_id')->unsigned()->index();
            $table->integer('closer_room_id')->unsigned()->index();
            $table->timestamps();

            $table->primary('room_id', 'closer_room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('set null');
            $table->foreign('closer_room_id')->references('id')->on('rooms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_room_pivot');
    }
}
