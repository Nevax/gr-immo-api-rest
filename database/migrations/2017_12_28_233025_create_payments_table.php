<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->date('date');
            $table->integer('invoices_id')->unsigned();
            $table->integer('payment_modes_id')->unsigned();
            $table->timestamps();

            $table->foreign('invoices_id')->references('id')->on('invoices')->onDelete('set null');
            $table->foreign('payment_modes_id')->references('id')->on('payment_modes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
