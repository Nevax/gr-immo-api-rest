<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('phone');
            $table->string('address');
            $table->string('zip_code');
            $table->string('city');
            // Owner
            $table->boolean('is_owner')->default('false');
            $table->integer('owner_agency_id')->unsigned()->nullable();
            // Buyer
            $table->boolean('is_buyer')->default('true');
            $table->integer('buyer_agency_id')->unsigned()->nullable();
            // Agent
            $table->boolean('is_agent')->default('false');
            $table->integer('agent_agency_id')->unsigned()->nullable();
            $table->integer('agent_chief_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();

            // Foreign key
            $table->foreign('owner_agency_id')->references('id')->on('agencies')->onDelete('set null');
            $table->foreign('buyer_agency_id')->references('id')->on('agencies')->onDelete('set null');
            $table->foreign('agent_agency_id')->references('id')->on('agencies')->onDelete('set null');
            $table->foreign('agent_chief_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
