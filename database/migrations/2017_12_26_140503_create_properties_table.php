<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->text('description');
            $table->float('size');
            $table->boolean('isSold');
            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();
            $table->float('price');
            $table->integer('property_type_id')->unsigned()->nullable();
            $table->integer('agency_id')->unsigned()->nullable();
            $table->integer('owner_id')->unsigned()->nullable();

            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('set null');
            $table->foreign('agency_id')->references('id')->on('agencies')->onDelete('set null');
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
